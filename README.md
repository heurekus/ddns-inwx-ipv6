# ddns-inwx-ipv6
Dynamic DNS Updater for INWX with IPv6

## Overview

INWX (https://www.inwx.com) offers XML, API and plain HTTP GET interfaces to update a DynDNS record. This script uses the **simple** HTTP GET method which requires the subdomain to be registered as a DynDNS subdomain in the INWX gui.

**IMPORTANT**: This script ONLY WORKS on hosts that have a **public IPv6** address as the PUBLIC IPv6 address that is extracted from the local interface overview is used to decide whether the DynDNS server needs to be updated. ADVANTAGE: No external server needs to be queried for the public IPv6 address in front of a NAT while the IPv6 prefix remains the same!

**IMPORTANT**: A individual username and password has to be configured for the subdomain, i.e. the username/pwd used are NOT the INWX account credentials!!!!

## Usage

Populate the *username*, *password* and *domain* and run the script. 
