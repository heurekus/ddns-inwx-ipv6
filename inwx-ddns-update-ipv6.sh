#!/bin/bash

#
# inwx-dyndns-update.sh
#
# @version    1.0.0 2018-05-04
# @copyright  Copyright (c) 2018 Martin Sauter, martin.sauter@wirelessmoves.com
# @license    GNU General Public License v2
# @since      Since Release 1.0
#
# Updates the DynDNS IPv4 and IPv6 records for a domain hosted at INWX.COM
#
# OVERVIEW: INWX offers XML, API and plain HTTP GET interfaces to update the DynDNS
# record. This script uses the HTTP GET method which requires the subdomain to be
# registered as a DynDNS subdomain in the INWX gui.
#
# IMPORTANT: This script ONLY WORKS on hosts that have a public IPv6 address
# as the PUBLIC IPv6 address that is extracted from the local interface overview
# is used to decide whether the DynDNS server needs to be updated.
# ADVANTAGE: No external server needs to be queried while the IPv6 prefix
# remains the same!
#
# IMPORTANT: A individual username and password has to be configured for the
# subdomain, i.e. the username/pwd used are NOT the INWX account credentials!!!!
#
# Version History
#
# 1.0.0 Initial version
#

# Configuration #########################################################

username="xxxx"
password="xxxx"

#Domain only required for saving the current ipv6 address to a file
domain="xxxx.de"

#########################################################################

# Servers that respond with the putlic IPv4 address from where the
# HTTP request originated from.
ipv4srv_1="https://ip4.nnev.de/"
ipv4srv_2="http://v4.ident.me/"
ipv4srv_3="https://ipv4.icanhazip.com"

echo '....................................................'
date

file=$HOME"/.inwx--"$domain

# Get previous IPv6 address from file
ipv6_old=`cat $file`

# Get current IPv6 address and check if it is still the same
######################################################

ipv6=$(ip -6 addr list scope global | grep -v " fd" | grep "global dynamic" | sed -n 's/.*inet6 \([0-9a-f:]\+\).*/\1/p' | head -n 1)
echo "Current IPv6 address: "$ipv6

if [ "$ipv6_old" = "$ipv6" ]; then
  echo "IPv6 address unchanged"
  exit 0
fi

# Get IPv4 address
###################

ipv4=$(curl -s "$ipv4srv_1")

if [ -z "$ipv4" ]; then
  echo "No IPv4 address found, trying alternate"

  ipv4=$(curl -s "$ipv4srv_2")
  if [ -z "$ipv4" ]; then
    echo "Again no IPv4 found, trying alternate..."
    ipv4=$(curl -s "$ipv4srv_3")
    if [ -z "$ipv4" ]; then
      echo "Again no IPv4 found, giving up..."
      ipv4="127.0.0.1"
    fi
  fi
fi

echo "Current IPv4 address: "$ipv4

authorization=$username":"$password
authorization=$(echo $authorization | base64)
authorization="Authorization: Basic "$authorization

#echo $authorization

upresult=$(curl -s "https://dyndns.inwx.com/nic/update?myip=$ipv4&myipv6=$ipv6" -H "$authorization")

echo "Update Result: "$upresult

# If the update was successful write IPv6 address to a file so no new ipv6 DNS registration
# is done in the next iteration

if [ -n "$upresult" ]; then
  echo $ipv6 > $file
fi

echo "done..."
